import smv2/command
import smv2/firmware
import smv2/defs

import tables

type
    Firmware1* = ref object of Firmware
        # Runtime memory
        params: Table[uint16, uint32]

        # Emulates the NV-memory
        memory: Table[uint16, uint32]

proc restart(firmware: Firmware1)
proc initMemory(firmware: Firmware1)

proc newFirmware1*(): Firmware1 =
    new(result)
    result.params = initTable[uint16, uint32]()
    result.memory = initTable[uint16, uint32]()
    result.initMemory()
    result.restart()

proc setCumulativeStatus[T: Ordinal](firmware: Firmware1, status: T) =
    firmware.params[SMP_CUMULATIVE_STATUS] = firmware.params[SMP_CUMULATIVE_STATUS] or status.uint32

proc resetCumulativeStatus(firmware: Firmware1) =
    firmware.params[SMP_CUMULATIVE_STATUS] = SMP_CMD_STATUS_ACK

type
    ParameterKind = enum
        ReadOnlyParam
        ChangeableParam
        RuntimeParam

    Parameter = tuple
        kind: ParameterKind
        value: uint32

template MakeReadOnlyParam(val: untyped = 0): Parameter =
    (kind: ReadOnlyParam, value: val.uint32)

template MakeChangeableParam(val: untyped = 0): Parameter =
    (kind: ChangeableParam, value: val.uint32)

template MakeRuntimeParam(val: untyped = 0): Parameter =
    (kind: RuntimeParam, value: val.uint32)

const
    supportedParams = {
        # Base
        SMP_CUMULATIVE_STATUS: MakeRuntimeParam(),
        SMP_RETURN_PARAM_ADDR: MakeRuntimeParam(),
        SMP_RETURN_PARAM_LEN: MakeRuntimeParam(),

        # Device specific
        SMP_STATUS: MakeReadOnlyParam(),
        SMP_SYSTEM_CONTROL: MakeRuntimeParam(),
        SMP_FIRMWARE_VERSION: MakeReadOnlyParam(0x00c0ffee),
        SMP_FIRMWARE_BUILD_REVISION: MakeReadOnlyParam(),
        SMP_DEVICE_TYPE: MakeReadOnlyParam(),

        SMP_ABSPOSITION_HI_LIMIT: MakeChangeableParam(),
        SMP_ABSPOSITION_LO_LIMIT: MakeChangeableParam(),
    }.toTable()

proc initMemory(firmware: Firmware1) =
    for id, param in supportedParams.pairs():
        firmware.memory[id.uint16] = param.value

proc restart(firmware: Firmware1) =
    firmware.params = firmware.memory

proc cmdReturnParam(fw: Firmware1, value: uint32): SimpleMotionCommand =
    # value contains the address of the value to return
    if not (value.uint16 in fw.params):
        # we don't have such a param
        result = SimpleMotionCommand(id: SM_RETURN_STATUS,
            value: SMP_CMD_STATUS_INVALID_ADDR)
    else:
        # return the value
        let paramLen = fw.params[SMP_RETURN_PARAM_LEN].byte
        result = SimpleMotionCommand(id: paramLen,
            value: fw.params[value.uint16])

proc cmdSystemControl(fw: Firmware1, value: uint32): SimpleMotionCommand =
    result = SimpleMotionCommand(id: SM_RETURN_STATUS, value: 0)

    case value
    of SMP_SYSTEM_CONTROL_SAVECFG:
        for id, param in supportedParams.pairs():
            if param.kind == ChangeableParam:
                fw.memory[id.uint16] = fw.params[id.uint16]
    of SMP_SYSTEM_CONTROL_RESTART:
        fw.restart()
    of SMP_SYSTEM_CONTROL_RESTORE_SAVED_CONFIG:
        for id, param in supportedParams.pairs():
            if param.kind == ChangeableParam:
                fw.params[id.uint16] = fw.memory[id.uint16]
    else:
        result.value = SMP_CMD_STATUS_INVALID_VALUE

const commands = {
    SMP_RETURN_PARAM_ADDR: cmdReturnParam,
    SMP_SYSTEM_CONTROL: cmdSystemControl
}.toTable()

method writeValue(fw: Firmware1, address: uint16, value: uint32): SimpleMotionCommand =
    if address in fw.params:
        if address.int in commands:
            result = commands[address.int](fw, value)
        else:
            result.id = SM_RETURN_STATUS
            if supportedParams[address.int].kind == ReadOnlyParam:
                result.value = SMP_CMD_STATUS_NACK
            else:
                fw.params[address] = value
                result.value = SMP_CMD_STATUS_ACK
    else:
        # we don't support writing to undefined addresses
        result = SimpleMotionCommand(id: SM_RETURN_STATUS,
            value: SMP_CMD_STATUS_INVALID_ADDR)

    if result.id == SM_RETURN_STATUS:
        fw.setCumulativeStatus(result.value)
