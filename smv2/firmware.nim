import command
import defs

type
    Firmware* = ref object of RootObj
        # params: TableRef[uint16, uint32]

method writeValue*(fw: Firmware, param: uint16, value: uint32): SimpleMotionCommand =
    raise newException(Exception, "not implemented")

