import packet
import command
import device

import tables
import streams

type
    SimpleMotionBus* = ref object
        devices*: seq[SimpleMotionDevice]
        parser: SimpleMotionPacketParser

    # SimpleMotionDeviceWriteValueCallback* = proc(device: SimpleMotionDevice,
    #     param: uint16, value: uint32): SimpleMotionCommand

    # SimpleMotionDevice* = ref object
    #     bus*: SimpleMotionBus
    #     parser: SimpleMotionCommandParser
    #     writeAddress: uint16
    #     params: TableRef[uint16, uint32]
    #     writeValueCallback*: SimpleMotionDeviceWriteValueCallback

proc newSimpleMotionBus*(): SimpleMotionBus =
    new(result)
    result.devices = @[]
    result.parser = newSimpleMotionPacketParser()

proc addDevice*(bus: SimpleMotionBus, device: SimpleMotionDevice) =
    bus.devices.add(device)

# proc setWriteValueCallback*(bus: SimpleMotionBus,
#                             cb: SimpleMotionDeviceWriteValueCallback) =
#     for device in bus.devices:
#         device.writeValueCallback = cb

# proc process*(device: SimpleMotionDevice, commands: seq[SimpleMotionCommand]): StringStream

proc feed*(bus: SimpleMotionBus, b: byte, response: var SimpleMotionPacket): bool =
    result = false

    if bus.parser.feed(b):
        let node = bus.parser.packet.node.int

        # Don't respond if the node does not exist
        if node == 0 or node > len(bus.devices):
            echo("non-existing node")
            bus.parser.resetState()
            return

        let commands = parseSimpleMotionCommands(bus.parser.packet.payload,
            bus.parser.packet.size.int)

        var responsePayload = bus.devices[node-1].process(commands)

        response.id = bus.parser.packet.id or 1
        response.node = bus.parser.packet.node
        response.size = len(responsePayload.data).byte
        copyMem(addr response.payload[0], addr responsePayload.data[0],
            len(responsePayload.data))

        result = true

        bus.parser.resetState()
