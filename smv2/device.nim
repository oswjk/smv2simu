import command
import firmware

import streams

type
    SimpleMotionDevice* = ref object
        parser: SimpleMotionCommandParser
        writeAddress: uint16
        firmware*: Firmware

proc newSimpleMotionDevice*(fw: Firmware): SimpleMotionDevice =
    new(result)
    result.parser = newSimpleMotionCommandParser()
    result.firmware = fw

proc process*(device: SimpleMotionDevice, command: SimpleMotionCommand): SimpleMotionCommand

proc process*(device: SimpleMotionDevice, commands: seq[SimpleMotionCommand]): StringStream =
    result = newStringStream()
    for command in commands:
        let resp = device.process(command)
        result.write(resp)

proc process*(device: SimpleMotionDevice, command: SimpleMotionCommand): SimpleMotionCommand =
    case command.id
    of SimpleMotionCommandSetWriteAddress:
        echo("SimpleMotionCommandSetWriteAddress ", command.value)
        device.writeAddress = command.value.uint16
        result = SimpleMotionCommand(id: SimpleMotionReturnStatus, value: 0)
    of SimpleMotionCommandWrite24, SimpleMotionCommandWrite32:
        result = device.firmware.writeValue(device.writeAddress, command.value)
    else:
        raise newException(Exception, "unsupported command")
