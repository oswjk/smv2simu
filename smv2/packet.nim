import streams
import crc

type
    SimpleMotionPacket* = object
        id*: byte
        node*: byte
        size*: byte
        payload*: array[0..120, byte]

    State = enum
        WaitCmdId
        WaitAddr
        WaitPayloadSize
        WaitPayload
        WaitCrcHi
        WaitCrcLo

    SimpleMotionPacketParser* = ref object
        state: State
        next: State
        crc: uint16
        receivedCrc: uint16
        payloadReceived: byte
        packet*: SimpleMotionPacket

proc paramCount*(packet: SimpleMotionPacket): int =
    case (packet.id and 0x6)
    of 0: result = 0
    of 2: result = 2
    of 4: result = -1
    else: raise newException(Exception, "invalid packet")

proc isFixedSize*(packet: SimpleMotionPacket): bool =
    result = (packet.paramCount() >= 0)


proc resetState*(parser: SimpleMotionPacketParser) =
    parser.state = WaitCmdId
    parser.next = WaitCmdId
    parser.payloadReceived = 0
    parser.crc = 0
    parser.receivedCrc = 0

proc newSimpleMotionPacketParser*(): SimpleMotionPacketParser =
    new(result)
    resetState(result)

proc feed*(parser: SimpleMotionPacketParser, b: byte): bool =
    result = false
    parser.state = parser.next

    case parser.state
    of WaitCrcHi: discard
    of WaitCrcLo: discard
    else: parser.crc = crc16(b, parser.crc)

    case parser.state
    of WaitCmdId:
        parser.packet.id = b
        if parser.packet.isFixedSize():
            parser.packet.size = b and 0x6
            parser.next = WaitAddr
        else:
            parser.next = WaitPayloadSize
    of WaitAddr:
        parser.packet.node = b
        if parser.packet.size > 0.byte:
            parser.next = WaitPayload
        else:
            parser.next = WaitCrcHi
    of WaitPayloadSize:
        parser.packet.size = b
        parser.next = WaitAddr
        # TODO: check size, raise if not valid
    of WaitPayload:
        if parser.payloadReceived < parser.packet.size:
            parser.packet.payload[parser.payloadReceived] = b
            inc(parser.payloadReceived)
        if parser.payloadReceived == parser.packet.size:
            parser.next = WaitCrcHi
    of WaitCrcHi:
        parser.receivedCrc = b
        parser.next = WaitCrcLo
    of WaitCrcLo:
        parser.receivedCrc = (parser.receivedCrc shl 8) or b.uint16
        if parser.crc != parser.receivedCrc:
            raise newException(Exception, "checksum mismatch")

    if parser.state == WaitCrcLo:
        result = true

proc writeByteWithCrc(s: Stream, b: byte, crc: var uint16) =
    s.write(b)
    crc = crc16(b, crc)

proc write*(s: Stream, packet: SimpleMotionPacket) =
    var
        crc: uint16

    echo("write packet.id:", packet.id)
    echo("write packet.size:", packet.size)
    echo("write packet.node:", packet.node)

    s.writeByteWithCrc(packet.id, crc)

    if (packet.id and 4) != 0:
        s.writeByteWithCrc(packet.size, crc)

    s.writeByteWithCrc(packet.node, crc)

    for i in 0..packet.size-1:
        s.writeByteWithCrc(packet.payload[i], crc)

    s.write((crc shr 8).byte)
    s.write((crc and 0xff).byte)
