{. compile: "crc_.c" .}

const arrayDummySize = when defined(cpu16): 10_000 else: 100_000_000
type UncheckedArray {.unchecked.} [T] = array[0..arrayDummySize, T]

var
    table_crc16_hi {. importc .}: UncheckedArray[byte]
    table_crc16_lo {. importc .}: UncheckedArray[byte]

proc crc16*(b: byte, crc: uint16): uint16 =
    let i = (crc shr 8) xor b.uint16
    let hi = table_crc16_hi[i].uint16
    let lo = table_crc16_lo[i].uint16
    result = (((crc and 0xff) xor hi) shl 8) or lo
