import streams

const
    SimpleMotionCommandSetWriteAddress* = 0x2
    SimpleMotionCommandWrite24* = 0x1
    SimpleMotionCommandWrite32* = 0x0

    SimpleMotionReturnStatus* = 0x3
    SimpleMotionReturnValue16* = 0x2
    SimpleMotionReturnValue24* = 0x1
    SimpleMotionReturnValue32* = 0x0

type
    SimpleMotionCommand* = object
        id*: byte
        value*: uint32

    # TODO:
    # SimpleMotionReturn* = distinct SimpleMotionCommand

    ParserState = enum
        WaitCommandId
        WaitPayload

    SimpleMotionCommandParser* = ref object
        state: ParserState
        next: ParserState
        shift: int
        command*: SimpleMotionCommand

proc reset(parser: SimpleMotionCommandParser) =
    parser.state = WaitCommandId
    parser.next = WaitCommandId
    parser.shift = 0

proc newSimpleMotionCommandParser*(): SimpleMotionCommandParser =
    new(result)
    result.reset()

proc feed*(parser: SimpleMotionCommandParser, b: byte): bool =
    result = false

    parser.state = parser.next

    echo("feed b:", b)

    case parser.state
    of WaitCommandId:
        parser.command.id = b shr 6
        parser.command.value = b and 0x3f
        parser.next = WaitPayload
        echo("WaitCommandId id:", parser.command.id, " value:", parser.command.value)
        # This will handle parsing both commands and return values
        case parser.command.id
        of 0x3: parser.shift = 0
        of 0x2: parser.shift = 1
        of 0x1: parser.shift = 2
        of 0x0: parser.shift = 3
        else: raise newException(Exception, "invalid command")
    of WaitPayload:
        echo("WaitPayload shift:", parser.shift, " old value:", parser.command.value)
        if parser.shift > 0:
            parser.command.value = parser.command.value shl 8
            dec(parser.shift)
        parser.command.value = parser.command.value or b.uint32
        echo("WaitPayload new value:", parser.command.value)
        result = (parser.shift == 0)
        if result:
            parser.reset()

    if result:
        echo("---")

proc parseSimpleMotionCommands*(payload: array[0..120, byte], size: int): seq[SimpleMotionCommand] =
    result = @[]

    var
        parser = newSimpleMotionCommandParser()
        nparsed = 0

    echo("parseSimpleMotionCommands size:", size)

    while nparsed < size:
        if parser.feed(payload[nparsed]):
            echo("parsed command ", parser.command.id)
            result.add(parser.command)
        else:
            if nparsed == size-1:
                raise newException(Exception, "malformed command")
        inc(nparsed)

proc write*(s: Stream, cmd: SimpleMotionCommand) =
    var
        data: array[0..3, byte]
        size = 0

    case cmd.id
    of 0x3:
        data[0] = ((cmd.id and 3) shl 6) or (cmd.value.byte and 0x3f)
        size = 1
    of 0x2:
        data[0] = ((cmd.id and 3) shl 6) or ((cmd.value shr 8).byte and 0x3f)
        data[1] = (cmd.value and 0xff).byte
        size = 2
    of 0x1:
        data[0] = ((cmd.id and 3) shl 6) or ((cmd.value shr 16).byte and 0x3f)
        data[1] = ((cmd.value shr 8) and 0xff).byte
        data[2] = (cmd.value and 0xff).byte
        size = 3
    of 0x0:
        data[0] = ((cmd.id and 3) shl 6) or ((cmd.value shr 24).byte and 0x3f)
        data[1] = ((cmd.value shr 16) and 0xff).byte
        data[2] = ((cmd.value shr 8) and 0xff).byte
        data[3] = (cmd.value and 0xff).byte
        size = 4
    else:
        raise newException(Exception, "invalid command id")

    echo("write command size:", size)

    for i in 0..size-1:
        echo("write command byte ", data[i])
        s.write(data[i])
