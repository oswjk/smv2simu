import smv2/packet
import smv2/command
import smv2/bus
import smv2/device

import net
import strutils
import streams

import firmware1

var sock = newSocket()
sock.setSockOpt(OptReuseAddr, true)
sock.bindAddr(Port(4001))
sock.listen()

var client = newSocket()
var address = ""

proc recvByte(sock: Socket): byte =
    let nbytes = sock.recv(result.addr, 1)
    if nbytes != 1:
        raise newException(Exception, "failed to read byte")

var smbus = newSimpleMotionBus()

var dev1 = newSimpleMotionDevice(newFirmware1())
smbus.addDevice(dev1)

var dev2 = newSimpleMotionDevice(newFirmware1())
smbus.addDevice(dev2)

while true:
    sock.acceptAddr(client, address)

    echo("client connected: ", address)

    var response: SimpleMotionPacket

    # TODO: handle the case when a client disconnect abrubtly. What happens to
    # the data fed to the parser. The original system doesn't have "connections"
    # as TCP/IP has, so should we just keep feeding it data the next client
    # sends us. What to do when the inevitable parsing error comes?

    while true:
        var data: byte
        try:
            data = client.recvByte()
        except:
            break
        if smbus.feed(data, response):
            var ss = newStringStream()
            ss.write(response)
            client.send(ss.data)

sock.close()
